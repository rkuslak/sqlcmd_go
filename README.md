SqlCmdgo
===
SqlCmdGo is an attempt to write a simple SQL Server "replacement" for sqlcmd in
Go. It features the ability to run batched queries respecting sqlcmds "GO"
notation in script files to seperate and repeat batches.

I started writing this as I was going through some old projects to move onto
GitHub and found a TSQL batch parser I wrote in C# that I thought would be a fun
experiment to convert in to Go. At the time I regularly worked with sites where
having access to a means to run TSQL scripts against a server I'm working on
isn't a given, having a small static executable I could use to run them was
something I thought would be useful.

Configuration Files
===
Full command line usage information is available using the --help option. A
configuration file is supplied as an example, you can call this by placing a
file named 'sqlcmd.cfg' within the current working directory of the program
on call, and defining one or several connections within. You will need to
specify which connection to use via command line option to use at runtime, for
example to use a connection named 'Foo' you would pass "--connection=Foo".

Example Configuration File JSON Layout
===

```
{
    "Connections": [
        {
            "connectionname": "Example Connection",
            "server": "sqlserver",
            "instance": "SQLEXPRESS",
            "port": 1433,
            "database": "AdventureWorks2020",
            "username": "sa",
            "password": "aSecurePassword"
        }
    ]
}
```

# Roadmap

## 0.0.3
* Arbirary variable definition from command line for query replacement
* Windows integrated authentication?
* PRINT support (dependant on mssql library support)
* Add unit testing