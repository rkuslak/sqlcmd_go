package main

// sqlcmd - a simple program that attempts to provide a static executable
// similar to sqlcmd but that allows for saving "encrypted" connection
// information suitable to use on low-security servers.
//
// Copyright (C) 2018 Ron Kuslak
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation version 2.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// QueryBox - A object that stores and modifies a array of query strings.
type QueryBox struct {
	sizeCurrent uint
	sizeMax     uint
	sizeIdx     uint
	queries     []string
}

// NewQueryBox - initializes a new QueryBox object.
func NewQueryBox() QueryBox {
	qb := QueryBox{
		sizeCurrent: 0,
		sizeIdx:     0,
		sizeMax:     10,
		queries:     make([]string, 10)}

	return qb
}

// GrowBox - Changes the size of the queries array to the passed size.
func (qb *QueryBox) GrowBox(newSize uint) {
	if newSize == qb.sizeCurrent {
		return
	}

	newQueries := make([]string, newSize)
	var x uint
	for x = 0; x < newSize && x < qb.sizeIdx; x++ {
		newQueries[x] = qb.queries[x]
	}

	qb.queries = newQueries
	qb.sizeMax = newSize
}

// AppendQueryString - Appends the passed string to the QueryBox object.
func (qb *QueryBox) AppendQueryString(query string) {
	// Must have at least 1 remaining element to write to:
	if qb.sizeIdx >= qb.sizeMax-1 {
		qb.GrowBox(qb.sizeMax * 2)
	}

	qb.queries[qb.sizeIdx] = query
	qb.sizeIdx++
}

// AppendQueryRunes - Appends the passed []runes array as a string to the
// QueryBox object.
func (qb *QueryBox) AppendQueryRunes(query []rune) {
	qb.AppendQueryString(string(query))
}

// isWhiteSpace - returns true if white space, false otherwise
func isWhiteSpace(ch rune) bool {
	return ch == ' ' || ch == '\t'
}

// isWhiteSpace - returns true if white space, false otherwise
func isNewLine(ch rune) bool {
	return ch == '\n' || ch == '\r'
}

// isGoOrWhiteSpace - Tests if passed rune is a legal character for being in a
// GO statement, including being white space, ;, or a number.
func isGoOrWhiteSpace(ch rune) bool {
	return ch == 'g' || ch == 'G' ||
		ch == 'o' || ch == 'O' ||
		ch == ';' || isWhiteSpace(ch) ||
		(ch >= '0' && ch <= '9')
}

// ParseQuery - Parses query and turns it into array of batches
func ParseQuery(query string) []string {
	queries := NewQueryBox()
	queryText := []rune(query)

	// Starting character index of current query:
	queryIdx := 0

	for x := 0; x < len(queryText); {
		switch {
		case queryText[x] == '"' ||
			queryText[x] == '\'' ||
			queryText[x] == '[':
			// Single character block; advance until pair or EOF and advance to
			// next available character.
			matchChar := queryText[x]
			if queryText[x] == '[' {
				matchChar = ']'
			}
			if x > 0 && queryText[x-1] != '\\' {
				for x < len(queryText) && queryText[x] != matchChar {
					x++
				}
			}
			x++
		case queryText[x] == '-' && queryText[x+1] == '-':
			// Single line comment; loop until EOL or EOF.
			for x < len(queryText) && queryText[x] != '\n' {
				x++
			}
		case queryText[x] == '/' && queryText[x+1] == '*':
			// Block comment; loop until */ or EOF.
			x += 2
			for x+1 < len(queryText) && queryText[x] != '*' && queryText[x+1] != '/' {
				x++
			}
			x += 2
		case isNewLine(queryText[x]):
			// Newline; loop until end of next line AND contains 'GO [0-9]*;*$',
			// or until any non-white space, non-number, or non-G and non-O
			// character:

			// Loop until next character is NOT a new line:
			for x < len(queryText) && isNewLine(queryText[x]) {
				eolIdx := x
				x++

				// Check if next line is valid GO statement. Eat any whitespace
				// prior to next statement.
				for x < len(queryText)-1 && isWhiteSpace(queryText[x]) {
					x++
				}

				// If go statement, attempt to find a repeatition amount if any.
				var timesNeeded uint64

				if x <= len(queryText)-2 &&
					(queryText[x] == 'g' || queryText[x] == 'G') &&
					(queryText[x+1] == 'o' || queryText[x+1] == 'O') {
					// Begin evaluation after GO command
					x += 2

					// TODO: This feels like it should be simpler
					// Find numeric value to be added, if any:
					for ; x < len(queryText) &&
						// Break on EOL or semicolon
						!isNewLine(queryText[x]) &&
						// Break on comments
						!(x < len(queryText)-1 &&
							(queryText[x] == '-' && queryText[x+1] == '-')) &&
						// Parse if number, or continue seeking if white space as
						// long as we have not previously found a number to add
						((isWhiteSpace(queryText[x]) && timesNeeded == 0) ||
							(queryText[x] >= '0' && queryText[x] <= '9')); x++ {
						charVal := queryText[x] - '0'
						if charVal >= 0 && charVal <= 9 {
							timesNeeded *= 10
							timesNeeded += uint64(charVal)
						}
					}

					// If we are in a white space character, we will still need to
					// verify there are no additional non-comment characters prior
					// to newline. Advance to next non-whitespace character.
					for x < len(queryText) && isWhiteSpace(queryText[x]) {
						x++
					}

					// If we are in a line comment advance to EOL.
					if x < len(queryText)-1 &&
						(queryText[x] == '-' && queryText[x+1] == '-') {
						for x < len(queryText) && !isNewLine(queryText[x]) {
							x++
						}
					}

					// If eval failed due to it being a invalid GO statement we will
					// be on a character that does NOT A) start a comment or B) end
					// a line. Reset evals and indexs.
					if x < len(queryText) && !isNewLine(queryText[x]) {
						x = eolIdx + 1
						break
					}

					// Eat newline and semicolon if present.
					if x < len(queryText) && isNewLine(queryText[x]) {
						x++
					}

					if timesNeeded == 0 {
						timesNeeded = 1
					}

					// Add the query each time we are instructed to by the GO
					// statement.
					// Since we eat the newline, if multiple GO statement
					// follow each other it is possible we have a
					// negative-length "query". If so ignore.
					var iter uint64
					if eolIdx > queryIdx {
						qt := queryText[queryIdx:eolIdx]
						for iter = 0; iter < timesNeeded; iter++ {
							queries.AppendQueryRunes(qt)
						}
					}

					// Set start of next query as end of pervious GO statement
					queryIdx = x
					// break
				}

				// GO statement invalid or line is not a GO statement. Continue
				// normal evaluation at character past EOL marker.
				x = eolIdx + 1
			}
		default:
			x++
		}
	}
	// Append final query:
	queries.AppendQueryRunes(queryText[queryIdx:])

	// resize to exact size of containing queries and return:
	queries.GrowBox(queries.sizeIdx)
	return queries.queries
}
