package main

// sqlcmd - a simple program that attempts to provide a static executable
// similar to sqlcmd but that allows for saving "encrypted" connection
// information suitable to use on low-security servers.
//
// Copyright (C) 2018 Ron Kuslak
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation version 2.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

// Connection is a struct to hold Json data for our connections in the
// configuration file.
type Connection struct {
	ConnectionName string `json:"connectionname"`
	Server         string `json:"server"`
	Instance       string `json:"instance,omitempty"`
	Port           uint   `json:"port,omitempty"`
	Database       string `json:"database,omitempty"`
	Username       string `json:"username,omitempty"`
	Password       string `json:"password,omitempty"`
}

// ConfigFile is a struct to contain the results of parsing a configuration file
type ConfigFile struct {
	Connections []Connection `json:"Connections"`
}

// ServerName - A computed property of the SQLConnection struct to ease logging
// of server connections or errors
// TODO: Do we need to check for escapable chars?
func (cfg Connection) ServerName() string {
	if cfg.Instance == "" {
		return cfg.Server
	}

	return fmt.Sprintf("%s\\%s", cfg.Server, cfg.Instance)
}

// GetConnectionString - Returns a connection string for SQL Server based on
// the values in the passed SqlConnection object
func (cfg Connection) GetConnectionString() string {
	var conStr string
	server := cfg.ServerName()

	conStr = fmt.Sprintf("server=%s", server)

	if cfg.Port != 0 {
		conStr = fmt.Sprintf("%s;port=%d", conStr, cfg.Port)
	}
	if cfg.Database != "" {
		conStr = fmt.Sprintf("%s;database=%s", conStr, cfg.Database)
	}
	if cfg.Username != "" {
		conStr = fmt.Sprintf("%s;user id=%s", conStr, cfg.Username)
	}
	if cfg.Password != "" {
		conStr = fmt.Sprintf("%s;password=%s", conStr, cfg.Password)
	}

	return conStr
}

// ParseConfigEnv loads the environment variables containing (hopefully)
// configurationat information and attempts to return a configuration file
// struct from it
func ParseConfigEnv() Connection {
	logger.Fatal("NYI")
	return Connection{}
}

// ParseCommandLine attempts to load configuration options from the command line
// and returns a struct hopefully containing any options provided, and a path to
// config file if provided
func ParseCommandLine() (Connection, string, string, string) {
	logger.Info("Initializing command line options")
	configFilePath := flag.String("config", "sqlcmd.cfg", "Path to file storing connection information")
	server := flag.String("server", "localhost", "The server (and instance name) to connect to")
	port := flag.Uint("port", 0, "The port of the destination server the SQL Server instance will speak on")
	database := flag.String("database", "master", "Default database to connect to")
	instance := flag.String("instance", "", "Instance name (if not specified with server) to connect to")
	username := flag.String("user", "", "Username to use to connect to the database server")
	password := flag.String("pass", "", "Password to use to connect to the database server")
	connection := flag.String("connection", "", "The server (and instance name) to connect to")
	query := flag.String("query", "", "Full text of the query to run")
	file := flag.String("file", "", "File containing TSQL script to run")

	flag.Parse()

	if *file != "" {
		if fileData, err := ioutil.ReadFile(*file); err == nil {
			*query = string(fileData)
		}
	}

	return Connection{
		Server:   *server,
		Instance: *instance,
		Port:     *port,
		Database: *database,
		Username: *username,
		Password: *password,
	}, *configFilePath, *query, *connection
}

// ParseConfigFile loads the passed path as a file and attempts to parse a
// configuration file struct from it
func ParseConfigFile(path string) (ConfigFile, error) {
	var result ConfigFile

	if _, err := os.Stat(path); err == nil {
		path, _ = filepath.Abs(path)
		file, err := os.Open(path)
		if err != nil {
			return ConfigFile{}, fmt.Errorf("Failed to load passed config file %s", path)
		}
		defer file.Close()

		if err = json.NewDecoder(file).Decode(&result); err != nil {
			return ConfigFile{}, fmt.Errorf("Error parsing config file %s", path)
		}
	}

	return result, nil
}

// Merge takes a secondary child config struct and merges the non-nil fields
// with the values in this struct, returning the resulting combination
func (cfg *Connection) Merge(child Connection) Connection {
	result := *cfg

	if child.ConnectionName != "" {
		result.ConnectionName = child.ConnectionName
	}
	if child.Server != "" {
		result.Server = child.Server
	}
	if child.Instance != "" {
		result.Instance = child.Instance
	}
	if child.Port != 0 {
		result.Port = child.Port
	}
	if child.Database != "" {
		result.Database = child.Database
	}
	if child.Username != "" {
		result.Username = child.Username
	}
	if child.Password != "" {
		result.Password = child.Password
	}

	return result
}
