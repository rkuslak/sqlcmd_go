package main

import (
	"database/sql"
	"fmt"
	"strings"
)

// PrintTableFromRows - Prints the passed Rows object to stdout
// TODO: UUIDs appear mangled. Should convert backing store to interface{}?
func PrintTableFromRows(rows *sql.Rows) {
	var columnCount int

	columns, err := rows.Columns()
	if err != nil {
		logger.Warningf("Error reading results: %s", err)
		return
	}

	// Give empty column names a "name"
	for x := 0; x < len(columns); x++ {
		if columns[x] == "" {
			columns[x] = "< Unnamed >"
		}
	}

	columnCount = len(columns)
	columnSizes := make([]int, columnCount)
	var columnData [][]string

	// Collect values from query, and find largest item in each column:
	for rows.Next() {
		// To generalize the logic to read unknown columns, we cast to byte
		// then cast to string. The "reader" will be a array of pointers
		// to the landing byte stores.
		row := make([]string, columnCount)
		reader := make([]interface{}, columnCount)
		dataRaw := make([][]byte, columnCount)
		for idx := range reader {
			reader[idx] = &dataRaw[idx]
		}

		if err := rows.Scan(reader...); err != nil {
			logger.Errorf("(!!) Error parsing row:\n%s", err)
			return
		}

		// Add column data and update max column data size, if larger
		for idx, data := range dataRaw {
			if data == nil {
				row[idx] = "< NULL >"
			} else {
				tmp := string(data)
				row[idx] = strings.Replace(tmp, "\n", "\\n", -1)
			}
			if columnSizes[idx] < len(row[idx]) {
				columnSizes[idx] = len(row[idx])
			}
		}
		columnData = append(columnData, row)
	}

	for idx, colName := range columns {
		if columnSizes[idx] < len(colName) {
			columnSizes[idx] = len(colName)
		}
	}

	// Header
	fmt.Print("| ")
	for idx, colName := range columns {
		fmt.Print(colName)
		for y := len(colName); y < columnSizes[idx]; y++ {
			fmt.Print(" ")
		}
		fmt.Print(" | ")
	}
	fmt.Print("\n+")
	for idx := range columns {
		fmt.Print("=")
		for y := 0; y < columnSizes[idx]; y++ {
			fmt.Print("=")
		}
		fmt.Print("=+")
	}
	fmt.Print("\n")

	// Row data
	for _, row := range columnData {
		fmt.Print("| ")
		for x := 0; x < columnCount; x++ {
			fmt.Print(row[x])
			for y := len(row[x]); y < columnSizes[x]; y++ {
				fmt.Print(" ")
			}
			fmt.Print(" | ")
		}
		fmt.Print("\n")
	}
	fmt.Print("\n\n")
}
