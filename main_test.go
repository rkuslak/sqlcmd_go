package main

// sqlcmd - a simple program that attempts to provide a static executable
// similar to sqlcmd but that allows for saving "encrypted" connection
// information suitable to use on low-security servers.
//
// Copyright (C) 2018 Ron Kuslak
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation version 2.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import (
	"testing"
)

// TestSQLConnection - Test that SQLConnection structure properly reports
// connection string based on data presented
func TestSQLConnection(t *testing.T) {
	tests := []struct {
		name string
		sql  SQLConnection
		want string
	}{
		{"Basic - Server and database",
			SQLConnection{Server: "test-server.com", Database: "datawherehouse"},
			"server=test-server.com;database=datawherehouse"},
		{"With user",
			SQLConnection{Server: "test-server.com", Database: "datawherehouse", Username: "sa", Password: "SecurePassword"},
			"server=test-server.com;database=datawherehouse;user id=sa;password=SecurePassword"},
		{"With instance",
			SQLConnection{Server: "test-server.com", Database: "datawherehouse", Instance: "SQLEXPRESS"},
			"server=test-server.com\\SQLEXPRESS;database=datawherehouse"},
		{"With port",
			SQLConnection{Server: "test-server.com", Database: "datawherehouse", Port: 12},
			"server=test-server.com;port=12;database=datawherehouse"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.sql.GetConnectionString(); got != tt.want {
				t.Errorf("Test %s: GetConnectionString() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}

// testSlices - Tests 2 passed slices to ensure parity between the values
// of the two. Fails if the size of the 2 slices varies or any value does
// not match at the same position.
func testSlices(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for x := 0; x < len(a); x++ {
		if a[x] != b[x] {
			return false
		}
	}
	return true
}

// TestGoParser - Tests the GO statement parsing logic for various generally
// expected queries.
func TestGoParser(t *testing.T) {
	tests := []struct {
		name  string
		query string
		want  []string
	}{
		{"Query #0",
			"SELECT 'Foo' FROM [dbo].[BAR]\n\nGO 3\nGO\n--GO\nGO",
			[]string{
				"SELECT 'Foo' FROM [dbo].[BAR]\n",
				"SELECT 'Foo' FROM [dbo].[BAR]\n",
				"SELECT 'Foo' FROM [dbo].[BAR]\n",
				"--GO",
				// As last line is the go statement, technically will be
				// empty line.
				""},
		},
		{"Query #1",
			"SELECT 'Foo'\nSELECT 'Bar'\n-- Test\nGO 2\ngo\nSELECT 'Foo'\nFROM 'Bar'",
			[]string{
				"SELECT 'Foo'\nSELECT 'Bar'\n-- Test",
				"SELECT 'Foo'\nSELECT 'Bar'\n-- Test",
				"SELECT 'Foo'\nFROM 'Bar'",
			},
		},
		{"Query #2",
			"SELECT 'Foo'\nSELECT 'Bar'\n-- Test",
			[]string{
				"SELECT 'Foo'\nSELECT 'Bar'\n-- Test",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ParseQuery(tt.query); !testSlices(got, tt.want) {
				t.Errorf("Test %s: ParseQuery() = %v, want %v", tt.name, got, tt.want)
			} else {
				t.Logf("Test passed: %s", tt.name)
			}
		})
	}
}
