package main
// sqlcmd - a simple program that attempts to provide a static executable
// similar to sqlcmd but that allows for saving "encrypted" connection
// information suitable to use on low-security servers.
//
// Copyright (C) 2018 Ron Kuslak
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation version 2.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import (
	"os"
	"github.com/op/go-logging"
)

var logger = logging.MustGetLogger("sqlcmd")

func initializeLogging() {
	// Initialze logging
	log1 := logging.NewLogBackend(os.Stderr, "", 0)
	log2 := logging.NewLogBackend(os.Stderr, "", 0)

	log1Level := logging.AddModuleLevel(log1)
	log1Level.SetLevel(logging.ERROR, "")

	logging.SetBackend(log1Level, log2)
}
