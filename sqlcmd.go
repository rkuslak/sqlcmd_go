package main

// sqlcmd - a simple program that attempts to provide a static executable
// similar to sqlcmd but that allows for saving "encrypted" connection
// information suitable to use on low-security servers.
//
// Copyright (C) 2018 Ron Kuslak
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation version 2.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import (
	"database/sql"

	_ "github.com/denisenkom/go-mssqldb"
)

// load command line options:
func loadCommandLineOptions() (Connection, []string) {
	var queries []string

	cfg, cfgPath, query, connectionName := ParseCommandLine()

	if cfgPath != "" {
		configFileData, err := ParseConfigFile(cfgPath)
		if err != nil {
			logger.Fatal(err)
		}
		for _, connectionData := range configFileData.Connections {
			if connectionData.ConnectionName == connectionName {
				cfg = cfg.Merge(connectionData)
			}
		}
	}

	queries = ParseQuery(query)

	return cfg, queries
}

func main() {
	var connectionString Connection
	var queries []string

	initializeLogging()
	connectionString, queries = loadCommandLineOptions()

	db, err := sql.Open("sqlserver", connectionString.GetConnectionString())
	if err != nil {
		logger.Fatal("Error connecting")
	}
	defer db.Close()

	logger.Debugf("Connection to server %s created", connectionString.ServerName())

	for _, batch := range queries {
		rows, err := db.Query(batch)
		if err != nil {
			logger.Fatalf("Query execution failed:\n%s\nResponse:\n%s", batch, err)
		}

		defer rows.Close()

		PrintTableFromRows(rows)
	}
}
