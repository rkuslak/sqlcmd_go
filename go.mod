module sqlcmdgo

require (
	cloud.google.com/go v0.36.0 // indirect
	github.com/denisenkom/go-mssqldb v0.0.0-20190204142019-df6d76eb9289
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	golang.org/x/crypto v0.0.0-20190228161510-8dd112bcdc25 // indirect
)
